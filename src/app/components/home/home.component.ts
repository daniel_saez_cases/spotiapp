import { SpotifyService } from './../../services/spotify.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  newReleases: any[] = [];

  constructor(private spotiSrv: SpotifyService) {
  }

  ngOnInit(): void {
    this.spotiSrv.getNewReleases().subscribe((result: any) => {
      this.newReleases = result;
    })
  }

}
