import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  artistas: any[] = [];

  constructor(private spotiSrv: SpotifyService) { }

  ngOnInit(): void {
  }

  buscar(termino: string) {
    console.log(termino);
    this.spotiSrv.getArtists(termino).subscribe(resp => {
      console.log(resp);
      this.artistas = resp;
    })
    
  }
}
