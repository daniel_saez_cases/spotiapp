import { API_SPOTIFY } from './../constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TOKEN } from '../constants';



@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query: string) {

    const headers = new HttpHeaders({
      'Authorization': TOKEN
    });

    return this.http.get(API_SPOTIFY + query, { headers })
  }

  getNewReleases(): Observable<any> {
    return this.getQuery('browse/new-releases?limit=20')
    .pipe(map((data: any) => data.albums.items));
  }

  getArtists(termino: string): Observable<any> {
    return this.getQuery(`search?q=${termino}&type=artist&limit=12`)
    .pipe(map((data: any) => data.artists.items));
  }
}
